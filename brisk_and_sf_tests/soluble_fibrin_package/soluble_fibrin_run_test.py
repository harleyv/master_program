#soluble_fibrin_run_test
import os, sys
import subprocess
import time
import json
import threading
from pathlib2 import Path
from sf_units import sf_table
from run_sf_test import get_tube_ready_times, run_object_detection_script
from multiprocessing import Pool, Process, Queue, Event
from PyQt4.QtCore import *

root_path = Path(os.path.abspath(__file__)).parents[2]


def run_scripts(camera):

	tube_time_queue = Queue()
	endpoint_queue = Queue()
	finished_process_event = Event()
	
	# Keeps track of when the the tube ready script starts writing images
	#started_writing_images = Event()

	tube_ready_time_process = Process(target=get_tube_ready_times, args=(tube_time_queue, finished_process_event))
	tube_ready_time_process.start()

	# wait for images to start being written to the csv file
	#started_writing_images.wait()

	# Initialize reading threads 
	#read_top_images_thread = threading.Thread(target=camera.read_images, args=(0, False))
	#read_bottom_images_thread = threading.Thread(target=camera.read_images, args=(1, False))

	# start the thread for reading and displaying images
	#read_top_images_thread.start()
	#read_bottom_images_thread.start()

	# wait for the read images threads to end
	#read_top_images_thread.join()
	#read_bottom_images_thread.join()

	# Wait for a tube process to finish
	finished_process_event.wait()

	# Get index and time for first tube to finish and start object detection
	first_gpu_index, tube_ready_time = tube_time_queue.get()
	
	first_tube_obj_det_process = Process(target=run_object_detection_script, args=(first_gpu_index, tube_ready_time, endpoint_queue, False))
	first_tube_obj_det_process.start()

	# Start reading images for obj detection on the first tube that finished
	#first_tube_read_images_thread = threading.Thread(target=camera.read_images, args=(first_gpu_index, True))
	
	# wait 5 seconds to give time for obj det to load up
	#first_tube_read_images_thread.start()

	# wait for other tube to finish
	tube_ready_time_process.join()

	# Get index and time for second tube to finish and start object detection
	second_gpu_index, tube_ready_time = tube_time_queue.get()
	second_tube_obj_det_process = Process(target=run_object_detection_script, args=(second_gpu_index, tube_ready_time, endpoint_queue, True))
	second_tube_obj_det_process.start()

	# Start reading images for obj detection on the second tube that finished
	#second_tube_read_images_thread = threading.Thread(target=camera.read_images, args=(second_gpu_index, True))
	
	# wait 35 seconds to start since the object detection thread is also delayed by 30 plus loading time
	#second_tube_read_images_thread.start()

	# Get endpoint for first tube
	first_tube_obj_det_process.join()
	endpoint1 = endpoint_queue.get()
	
	# Get endpoint for second tube
	second_tube_obj_det_process.join()
	endpoint2 = endpoint_queue.get()

	# Wait for reading images threads to finish
	#first_tube_read_images_thread.join()
	#second_tube_read_images_thread.join()

	if first_gpu_index == 0:
		return (endpoint1, endpoint2)
	else:
		return (endpoint2, endpoint1)


def run_soluble_fibrin_test(camera):
	# Returns a tuple in the following format: (top tube endpoint, bottom tube endpoint)
	endpoints = run_scripts(camera)

	sf_units = sf_table(endpoints[0], endpoints[1])

	return endpoints, sf_units

#run_soluble_fibrin_test()
#print(get_tube_ready_times())
