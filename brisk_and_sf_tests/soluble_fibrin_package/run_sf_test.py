import os, sys
import subprocess
import json
from pathlib2 import Path
from sf_units import sf_table
from multiprocessing import Process, Queue, Event
import csv
import numpy as np
import base64
import cv2
import time
from PyQt4.QtCore import *
from PyQt4.QtGui import *

#Define current file path and root path
root_path = Path(os.path.abspath(__file__)).parents[2]
current_file_path = Path(os.path.abspath(__file__)).parents[0]
TEMP_CSV_PATH = str(root_path) + "/tmp/images_tube_"
TEMP_PATH = str(root_path) + "/tmp/"
#define path to tube_ready script
tube_ready_path = current_file_path.parents[0] / 'tube_ready_not_ready_analysis_convert_to_function.py'

def run_tube_ready_script(gpu_index, results_queue, tube_event):
	
    # Start tube ready subprocess
	tube_ready_subprocess = subprocess.Popen(["/home/bha/fastai36/bin/python3.6",str(tube_ready_path)], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.STDOUT)
    
	#time.sleep(5)
	#read_images(gpu_index, tubeSignal)
	
	tube_ready_seconds = tube_ready_subprocess.communicate(str(gpu_index).encode())[0].decode()

	tube_ready_seconds = 0

	filePath = TEMP_PATH + "tube_ready_" + str(gpu_index) + ".csv"

	with open(filePath, "r") as f:
		fileReader = csv.reader(f)
		row = next(fileReader)
		tube_ready_seconds = row[0]
		
	os.remove(filePath)

	print("tube ready seconds is")
	print(tube_ready_seconds)
    
	if not tube_event.is_set():
		tube_event.set()
		results_queue.put([gpu_index, tube_ready_seconds])
		return
    
	results_queue.put([gpu_index, tube_ready_seconds])

	return tube_ready_seconds

def run_object_detection_script(gpu_index, tube_ready_time, endpoint_queue, delay):
	soluble_fibrin_obj_det_subprocess = subprocess.Popen([str(current_file_path / "activate_tf_gpu")], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr = subprocess.STDOUT, shell=True)

    #data to be sent to object detection subprocess
	send_data = tube_ready_time + "/" + str(gpu_index) + "/" + str(int(delay))
    #send tube ready seconds through pipe to obj detection program
	obj_det_output = soluble_fibrin_obj_det_subprocess.communicate(send_data.encode())[0].decode()
    
    #get output from obj_det process, parse this to get actual endpoint
	obj_det_output = obj_det_output.split('\n')

	for line in obj_det_output:
		print(line)

	soluble_fibrin_endpoint = float(obj_det_output[len(obj_det_output)-2])

	endpoint_queue.put(soluble_fibrin_endpoint)

	return endpoint_queue

def get_tube_ready_times(tube_time_queue, finished_process_event):
	results_queue = Queue()
	tube_event = Event()
	
	top_tube_process = Process(target=run_tube_ready_script, args=(0, results_queue, tube_event))
	bottom_tube_process = Process(target=run_tube_ready_script, args=(1, results_queue, tube_event))
	
	# Start process for both tubes
	top_tube_process.start()
	bottom_tube_process.start()
	
	#time.sleep(5)
	#writing_event.set()


	# Wait for whichever process finishes first
	tube_event.wait()

	# Get the index and tube ready time of first process to finish
	output = results_queue.get()
	
	# Set the event to start the object detection
	if not finished_process_event.is_set():
		finished_process_event.set()
		tube_time_queue.put(output)

	top_tube_process.join()
	bottom_tube_process.join()

	output = results_queue.get()
	tube_time_queue.put(output)

#run_tube_ready_script(0, Queue(), Event())