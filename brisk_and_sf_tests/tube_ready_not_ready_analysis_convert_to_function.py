#use python 3.6 - run from fast.ai environment
#!/home/bha/fastai36/bin/activate
import os
import glob
import sys
from fastai.vision import *
import numpy as np
from PIL import Image, ImageDraw
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')
#matplotlib.use('Agg')
import cv2
from statsmodels.tsa.filters import hp_filter
from natsort import natsorted, ns
import time
from __main__ import *
from pathlib import Path
from multiprocessing import Process
import csv
import base64
gpu_index = int(input())
#gpu_index = 0
grandparent_path = Path(os.path.abspath(__file__)).parents[1]
current_file_path = Path(os.path.abspath(__file__)).parents[0]

CSV_FILE_PATH = str(grandparent_path) + "/tmp/images_tube_"
TEMP_PATH = str(grandparent_path) + "/tmp/"

def movingaverage (values, window):
        weights = np.repeat(1.0, window)/window
        sma = np.convolve(values, weights, 'valid')
        return sma

def tube_ready_not_ready_function(gpu_index):

    torch.cuda.set_device(gpu_index)
    csv_file_path = CSV_FILE_PATH + str(gpu_index) + ".csv"
    #path for non live testing
    #picture_path = Path('clotting_test_2')

    #path for live test
    cropped_pics_path = os.path.join(grandparent_path, "cropped_pics")

    fig, ax = plt.subplots()
    plt.get_current_fig_manager().resize(400,400)

    window_position = (0, 0)
    #window_name = 'Tube Ready / Not Ready' + str(gpu_index)
    
    if gpu_index == 0:
        cropped_pics_path = cropped_pics_path + '/top_tube'
        window_name = 'Tube Ready / Not Ready Right Tube'
        plt.get_current_fig_manager().window.wm_geometry("+1500+0")
        window_position = (980, 800)
        

    elif gpu_index == 1:
        cropped_pics_path = cropped_pics_path + '/bottom_tube'
        window_name = 'Tube Ready / Not Ready Left Tube'
        plt.get_current_fig_manager().window.wm_geometry("+1500+450")
 
        window_position = (40, 800)

    learn = load_learner(current_file_path, file='export_tube_not_ready_may14.pkl')
    classes = ['tube_ready','tube_not_ready' ]
    full_clot_probability_array = []
    frame_number = 1

    endpoint = []
    frame_endpoint_time = []
    endpoint_final_frame_number = []
    #clotting_endpoint_time = []

    #numpy rolling averages function

    clotting_endpoint_time = 0
    num_analysis_done = 0
    seconds_array = [] 
    endpoint_from_trend = 0

    #fileWriter = csv.writer(open(csv_file_path, "w"))

    #fileWriter.writerow([len(os.listdir(cropped_pics_path))])

    #make graph object
    #fig, ax = plt.subplots()
    #plt.get_current_fig_manager().window.wm_geometry("+1300+0") # move the window    

    while True:
        if num_analysis_done == len(os.listdir(cropped_pics_path)):
            time.sleep(1)
            if num_analysis_done == len(os.listdir(cropped_pics_path)):
                time.sleep(1)
                if num_analysis_done == len(os.listdir(cropped_pics_path)):
                    files = glob.glob(cropped_pics_path + '/*')
                    for f in files:
                        #os.remove(f)
                        placeholder = 0
                    break

        files = natsorted(os.listdir(cropped_pics_path))
        filename = files[num_analysis_done]

        img = open_image(os.path.join(cropped_pics_path,filename))
        #mg.show()
  
        pred_class,pred_idx,outputs = learn.predict(img)
        #print(filename)

        #pred_class2,pred_idx2,outputs2 = learn.predict(imgM2)

        #this_image = os.path.join('/home/bha/Desktop/lluFolder/masterProgram/01_26_19/analysis/cropped_pics', filename)
        this_image = os.path.join(cropped_pics_path, filename)
        image_read = cv2.imread(this_image)
        

        resized_image = cv2.resize(image_read, (900, 200))
        #resized_image = cv2.resize(image_read, (1200, 250))
        
        #image = base64.b64encode(cv2.imencode('.jpg', resized_image)[1]).decode()

        #fileWriter.writerow([filename, image])
        
        cv2.imshow(window_name, resized_image)   
        cv2.moveWindow(window_name, window_position[0], window_position[1])

        #convert torch.Tensor to nparray
        #print(type(outputs))
        tensor_to_np = outputs.numpy()
        #print(tensor_to_np)
    
        #grab clot probability from each output
        grab_clot_probability = 1 - tensor_to_np[0]
        #print(grab_clot_probability)
    
        #add each new clot probability to end of array
      
        full_clot_probability_array.append(grab_clot_probability)
        #print("probability array is")
        #print(full_clot_probability_array)

        #get seconds for each frame, extract first whole integer set before .extension in "image"
        file = filename
        seconds = None
        position = file.index('.')
        #gets filename position two before last(.) until last(.)                      
        seconds = filename.split('.')
        seconds = seconds[1] + '.' + seconds[2]
        seconds = float(seconds)
        seconds_array.append(seconds)
   

        plt.title(window_name)
        #mngr = plt.get_current_fig_manager()
        #mngr.window.SetPosition(0,0)   
        #plt.xticks(np.arange(min(seconds_array), max(seconds_array)+1, 1.0))
        plt.plot(seconds_array, full_clot_probability_array)	
        plt.draw()
        plt.pause(.001)	
    


        #smooth graph/remove noise with hp filter; store the smoothed values in "trend", and plot on same graph
        if frame_number > 1:
            cycle, trend = hp_filter.hpfilter(full_clot_probability_array, lamb=100000)
            #print("trend is")
            #print(trend[-1])
            plt.plot(seconds_array, trend)
            plt.draw()
            plt.pause(.001)
            plt.clf()
    
        #numpy rolling average
        if frame_number > 1:
            clot_moving_average = movingaverage(full_clot_probability_array, 20)
            plt.plot(clot_moving_average)
            plt.draw()



        #use trend to find endpoint
        if frame_number >1 and endpoint_from_trend==0 and trend[-1] > .1:
            endpoint_from_trend = seconds_array[-1]
                       
           
        if endpoint_from_trend!=0:
            #print("endpoint frame is")  
            #print(endpoint_final_frame_number)       
            plt.axvline(x=endpoint_from_trend, color='r', linestyle='--')
            #stdout = sys.stdout.write(str(endpoint_from_trend))        
            break
   
        num_analysis_done = num_analysis_done +1
        frame_number = frame_number + 1
        cv2.waitKey(50)


    #fileWriter.writerow(["END"])
    endpoint_from_trend = str(endpoint_from_trend)
    sys.stderr.write(endpoint_from_trend)

    filePath = TEMP_PATH + "tube_ready_" + str(gpu_index) + ".csv"

    with open(filePath, "w+") as f:
        fileWriter = csv.writer(f)
        fileWriter.writerow([endpoint_from_trend])
        f.close()

tube_ready_not_ready_function(gpu_index)
