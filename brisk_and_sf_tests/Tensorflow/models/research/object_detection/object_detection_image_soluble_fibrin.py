#!/home/chris/anaconda3/envs/tf_gpu/bin/python3.7

# Import packages
import os
import cv2
import numpy as np
import time
import tensorflow as tf
import sys
from natsort import natsorted, ns
import math
import matplotlib
import json
import tkinter
import matplotlib.pyplot as plt
matplotlib.use('TkAgg', force=True)
#matplotlib.get_backend() 
#import plotly.graph_objects as go
import time
from scipy.interpolate import *
from statsmodels.tsa.seasonal import seasonal_decompose
from statsmodels.tsa.filters import hp_filter
from pathlib import Path
from multiprocessing import Process, Queue, Event

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")

# Import utilites
from utils import label_map_util
from utils import visualization_utils as vis_util

import csv
import base64

# Define Paths
root_path = Path(os.path.abspath(__file__)).parents[5]
current_file_path = Path(os.path.abspath(__file__)).parents[0]

#random_image_path = os.path.join(root_path, "cropped_pics/rand.jpg")

# Name of the directory containing the object detection module we're using
MODEL_NAME = 'data_sf'
IMAGE_NAME = 'crop1.jpg'

# Path to frozen detection graph .pb file, which contains the model that is used for object detection.
PATH_TO_CKPT = os.path.join(current_file_path,MODEL_NAME,'frozen_inference_graph.pb')

# Path to label map file
PATH_TO_LABELS = os.path.join(current_file_path,MODEL_NAME,'label_map.pbtxt')

# Number of classes the object detector can identify
NUM_CLASSES = 2

CSV_FILE_PATH = str(root_path) + "/tmp/images_tube_"

# Load the label map.
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

#numpy rolling averages function
def movingaverage (values, window):
    weights = np.repeat(1.0, window)/window
    sma = np.convolve(values, weights, 'valid')
    return sma

config = tf.ConfigProto()
#config.graph_options.optimizer_options.global_jit_level = tf.OptimizerOptions.ON_1
#config.gpu_options.per_process_gpu_memory_fraction = 0.5
config.gpu_options.allow_growth=True


def run_object_detection(tube_ready_time, gpu_index, delay):
	# Set which gpu to use
	os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu_index)	
	
	# Set the file path name for the csv file
	csv_file_path = CSV_FILE_PATH + str(gpu_index) + ".csv"


	sess = tf.Session(config=config)
	# Load the Tensorflow model into memory.
	detection_graph = tf.Graph()
	
	with detection_graph.as_default():
		od_graph_def = tf.GraphDef()
		with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
		    serialized_graph = fid.read()
		    od_graph_def.ParseFromString(serialized_graph)
		    tf.import_graph_def(od_graph_def, name='')

		sess = tf.Session(graph=detection_graph)
	

	
	# Define input and output tensors (i.e. data) for the object detection classifier

	# Input tensor is the image
	image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

	# Output tensors are the detection boxes, scores, and classes
	# Each box represents a part of the image where a particular object was detected
	detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

	# Each score represents level of confidence for each of the objects.
	# The score is shown on the result image, together with the class label.
	detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
	detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

	# Number of objects detected
	num_detections = detection_graph.get_tensor_by_name('num_detections:0')

	# Number of each class detected
	num_sf_clumps = []
	num_sf_gel = []

	#array, custom num of detections using scores
	number_of_boxes_drawn = []

	frame_number = 0
	frame_number_array = []
	endpoint = []
	frame_endpoint_time = []
	endpoint_final_frame_number = []
	clotting_endpoint_time = 0
	num_analysis_done = 0
	seconds_array = [] 
	endpoint_from_trend = 0

	#bulls algorithm
	sensitivity_variable = 15
	bull_avgs = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
	bull_new_avg = 0
	summation = 0
	is_fraction_negative = False
	num_detections_for = {'sf_clump': 0, 'sf_gel': 0}
	clump_detections = []
	gel_detections = []

	#receive starting point from soluble_fibrin_tube_ready
	print("starting point object detection")
	print(tube_ready_time)


	# Load image using OpenCV and
	# expand image dimensions to have shape: [1, None, None, 3]
	# i.e. a single-column array, where each item in the column has the pixel RGB value


	#FOR LOOP TO ITERATE THROUGH IMAGE DIRECTORY

	#live test images
	PATH_TO_IMAGES = os.path.join(root_path,'cropped_pics/top_tube')

	window_position = (0, 0)

	if gpu_index == 0:
		PATH_TO_IMAGES = os.path.join(root_path,'cropped_pics/top_tube')
		window_name = 'Tube Ready / Not Ready Right Tube'
		window_position = (980, 800)
	elif gpu_index == 1:
		PATH_TO_IMAGES = os.path.join(root_path,'cropped_pics/bottom_tube')
		window_name = 'Tube Ready / Not Ready Left Tube'
		window_position = (40, 800)
	
	obj_det_start_time = time.time()

	current_second = 0
	frames_viewed = 0
	
	#config.gpu_options.per_process_gpu_memory_fraction = 1
	#sess = tf.Session(config=config)
	
	start_time = time.time()

	if delay:
			time.sleep(30)

	#fileWriter = csv.writer(open(csv_file_path, "w"))

	#fileWriter.writerow([len(os.listdir(PATH_TO_IMAGES))])

	#for filename in natsorted(os.listdir(PATH_TO_IMAGES)):
	while True:
		if num_analysis_done == len(os.listdir(PATH_TO_IMAGES)):
			time.sleep(1)
			if num_analysis_done == len(os.listdir(PATH_TO_IMAGES)):
				time.sleep(1)
				if num_analysis_done == len(os.listdir(PATH_TO_IMAGES)):
					break
    
		files = natsorted(os.listdir(PATH_TO_IMAGES))
		filename = files[num_analysis_done] 

		num_analysis_done = num_analysis_done +1

		#get seconds for each frame, extract first whole integer set before .extension in "image"
		file = filename
		seconds = None
		position = file.index('.')
			#gets filename position two before last(.) until last(.)                      
		seconds = filename.split('.')
		seconds = seconds[1] + '.' + seconds[2]
		seconds = float(seconds)

		#skip all frames before "tube_ready_time" frame
		#print(filename)
		if seconds < float(tube_ready_time):
			#print("not analyzed")		
			continue
		
		#append seconds of frame tube_ready
		seconds_array.append(seconds)

		#print(filename)
		image = cv2.imread(os.path.join(PATH_TO_IMAGES, filename))
	
		image_expanded = np.expand_dims(image, axis=0)

		#print('Detection start time:  Iteration %d: %.3f sec'%(frame_number, time.time()-start_time))
		# Perform the actual detection by running the model with the image as input
		(boxes, scores, classes, num) = sess.run(
			[detection_boxes, detection_scores, detection_classes, num_detections],
			feed_dict={image_tensor: image_expanded})

		#print time of inference
		frame_number += 1
		frame_number_array.append(frame_number)
		#print('Detection End Time:  Iteration %d: %.3f sec'%(frame_number, time.time()-start_time))
	
		# Draw the results of the detection (aka 'visualize the results')
	
		min_score_thresh = 0.60

		image = cv2.resize(image, (900,200))
		vis_util.visualize_boxes_and_labels_on_image_array(
			image,np.squeeze(boxes),
			np.squeeze(classes).astype(np.int32),
			np.squeeze(scores),
			category_index,
			use_normalized_coordinates=True,
			line_thickness=2,
			min_score_thresh=0.60)

		# reset detections count
		num_detections_for['sf_clump'] = 0
		num_detections_for['sf_gel'] = 0

		#count the number of detections for each class
		for index, value in enumerate(classes[0]):
			if scores[0, index] > min_score_thresh:
				class_name = category_index.get(value).get('name')
				num_detections_for[class_name] += 1

		print("SF CLUMP CNT: " + str(num_detections_for['sf_clump']))
		print("SF GEL CNT:   " + str(num_detections_for['sf_gel']))
		print('')

		if gpu_index == 0:		
			window_name = 'Soluble Fibrin Detection: Right Tube'
		else:
			window_name = 'Soluble Fibrin Detection: Left Tube'

		cv2.namedWindow(window_name)

		if gpu_index == 0:
			cv2.moveWindow(window_name, 980, 800)
		else:
			cv2.moveWindow(window_name, 40, 800)

		cv2.imshow(window_name, image)

		# Write image to csv file

		# image = base64.b64encode(cv2.imencode('.jpg', image)[1]).decode()
		# fileWriter.writerow([filename, image])

		#count and display the number of boxes drawn on platelets at each frame
		number_of_boxes_drawn.append(np.count_nonzero(scores))	
		clump_detections.append(num_detections_for['sf_clump'])
		gel_detections.append(num_detections_for['sf_gel'])

		mngr = plt.get_current_fig_manager()
		mngr.resize(400,400)
		plt.title(window_name)


		if gpu_index == 0:
			#mngr.title("Right Tube")			
			mngr.window.wm_geometry("+1500+000")
            
		else:
			#mngr.title("Left Tube")
			mngr.window.wm_geometry("+1500+400")
            
		
		plt.plot(seconds_array, number_of_boxes_drawn)		
		plt.draw()

		#smooth graph/remove noise with hp filter; store the smoothed values in "trend"
		if frame_number > 5:
			cycle, trend = hp_filter.hpfilter(number_of_boxes_drawn, lamb=50)
			
			plt.plot(seconds_array, trend)	
			plt.draw()	
		
		    #use trend to find endpoint
		if frame_number >5 and endpoint_from_trend==0 and trend[-1] >= 5.5:
			endpoint_from_trend = seconds_array[-1]

		if endpoint_from_trend!=0:   
			plt.axvline(x=endpoint_from_trend, color='r', linestyle='--') 

		#bulls algorithm
		if frame_number > sensitivity_variable:
			summation = 0
			summation_squared = 0	
			fraction = 0
		
			is_outside_negative = False		
			
			for element in number_of_boxes_drawn[-sensitivity_variable:]:			
				is_fraction_negative = False			
		
				fraction = (element - bull_avgs[-1])
			
				if fraction < 0:
					fraction = abs(fraction)
					is_fraction_negative = True			
						
				sqrt_fraction = math.sqrt(fraction)
				if is_fraction_negative is True:	
					sqrt_fraction = -sqrt_fraction
			
				summation = summation + sqrt_fraction		


			divide_by_sensitivity = summation/sensitivity_variable

			if divide_by_sensitivity < 0: 
				is_outside_negative = True

			summation_squared = divide_by_sensitivity**2
			
			if is_outside_negative is True:	
				summation_squared = -summation_squared
			bull_new_avg = bull_avgs[-1] + summation_squared
			
			bull_avgs.append(bull_new_avg)
			
			bull_avg_x_value = [x-3 for x in seconds_array]
		
				
			plt.plot(bull_avg_x_value, bull_avgs)
			#plt.ylim(0, 5)	
			plt.draw()
			plt.pause(.001)
			plt.clf()		



		    #how to find where there is a + slope change for a long time(add 50 frames gradient)
		#find the max gradient	
		#if frame_number > 4020: 
			#i_max = np.argmax(np.abs(np.gradient(trend, 50)))
			#i_max = np.argmax(np.gradient(trend, 50))
		#	endpoint = [None]
			#print("argmax gradient is")
			#print(np.argmax(np.gradient(trend, 50)))		
		#	if not endpoint:
		#		if (np.argmax((np.gradient(trend, 50)) > 0.25)):
		#			endpoint = (np.argmax((np.gradient(trend, 50)) > 0.25))
			#print(i_max)
			#print(endpoint)		
			#print(frame_number[i_max]) 		

		#how to find where there is a slope change for a long time
		#find the max gradient


		#seasonal decompose
		#if count>200:
		#	platelet_trend = seasonal_decompose(number_of_boxes_drawn, freq=10, extrapolate_trend = 50)
		#	print(platelet_trend.trend)
		#	platelet_trend.plot()
		#	plt.show()
		#create rolling average
		#rolling_average = df[['number_of_boxes_drawn']]
		
	
	
		#polynomial wont work here bc the longer you wait, the more the line has to change, thus throwing off accuracy
		#create curve fit function, polynomial least squares
		#y = number_of_boxes_drawn

		# populate x values for polyfit with numbers of frames iterated	
		#x = []	
		#for i in range(0, len(number_of_boxes_drawn)):
		#	x.append(i)
	  
		#do polyfit, print out polyfit values and corresponding frame for that degree 
		#if count>10:
		
		#	p1 = np.polyfit(x,y,3)
		#	print(p1)
		#	#plt.plot(x,y,'o')
		#	plt.plot(x, np.polyval(p1,x),'r-')

		#numpy rolling average
		if frame_number > 5000:
			objects_moving_average = movingaverage(number_of_boxes_drawn, 5)
			if gpu_index == 0:
				plt.plot(objects_moving_average)
				plt.draw()
				plt.clf()
		
		cv2.waitKey(50)

	return str(endpoint_from_trend)


data = input()
data = data.split("/")
tube_ready_time = float(data[0])
gpu_index = int(data[1])
delay = bool(int(data[2]))

print(run_object_detection(tube_ready_time,gpu_index,delay))

os.environ["CUDA_VISIBLE_DEVICES"]= "0, 1"
