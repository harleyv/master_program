import os, sys
import subprocess
from pathlib2 import Path

def run_brisk_test():
    current_file_path = Path(os.path.abspath(__file__)).parents[0]
    
    subprocess.Popen([str(current_file_path) + '/brisk_activate_tf_gpu'], shell=True)
    
    brisk_clotting_analysis_path = str(current_file_path / 'brisk_clotting_analysis.py')

    clotting_subprocess = subprocess.Popen(["/home/bha/fastai36/bin/python3.6", brisk_clotting_analysis_path], stdout=subprocess.PIPE, stderr = subprocess.STDOUT)
    
    clotting_endpoint_time = (clotting_subprocess.communicate()[0])
    #print(clotting_endpoint_time)
    #clotting endpoing time is the printed output of "analysis script" only works if this is only outputting the endpoint(printing only one thing)
    clotting_endpoint_time = str(clotting_endpoint_time)			
	#clotting_endpoint_digits_only = ''.join(filter(lambda i: i.isdigit(), clotting_endpoint_time))	
	#window.logClot(clotting_endpoint_time)
